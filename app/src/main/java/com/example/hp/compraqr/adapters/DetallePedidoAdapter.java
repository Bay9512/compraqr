package com.example.hp.compraqr.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baydev.compraqr.R;

import com.example.hp.compraqr.entidades.Producto;

import java.util.ArrayList;
import java.util.List;

public class DetallePedidoAdapter extends RecyclerView.Adapter<DetallePedidoAdapter.VH> {
    private List<Producto> dataset;
    private View.OnClickListener clickListener;

    public DetallePedidoAdapter() {
        dataset = new ArrayList<>();
    }

    public void setDataset(List<Producto> dataset) {
        this.dataset = dataset;
        this.notifyDataSetChanged();
    }

    public List<Producto> getDatase(){
        return this.dataset;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto_lista,parent,false);
        view.setOnClickListener(clickListener);



        return new DetallePedidoAdapter.VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.setData(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void agregar(Producto producto) {
        if(!dataset.contains(producto)) {
            this.dataset.add(producto);
            notifyDataSetChanged();
        }
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvNomProducto, tvCantidadProd;

        private VH(View itemView) {
            super(itemView);
            tvNomProducto = itemView.findViewById(R.id.tvNomProducto);
            tvCantidadProd = itemView.findViewById(R.id.tvCantidadProd);
        }

        public void setData(final Producto producto) {

            tvNomProducto.setText(producto.getNombreProd());
            tvCantidadProd.setText(String.valueOf(producto.getCantidadProd()));
        }
    }

    public void setOnClickListener(View.OnClickListener clickListener){
        this.clickListener = clickListener;
    }
}
