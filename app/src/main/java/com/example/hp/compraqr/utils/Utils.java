package com.example.hp.compraqr.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.Html;
import android.text.Spanned;

import com.baydev.compraqr.R;

import com.example.hp.compraqr.entidades.Pedido;
import com.example.hp.compraqr.entidades.Producto;
import com.google.gson.Gson;

public class Utils {

    private static void showDialog(final Context contexto, final String title, final Spanned message) {
        new AlertDialog.Builder(contexto){{
            setTitle(title);
            setMessage(message);
            setPositiveButton(contexto.getString(R.string.acerca_de_btnAceptar), null);
        }}.create().show();
    }


    private static PackageInfo getPackageInfo(Context context)
    {
        PackageInfo pckginfo = null;
        try {
            pckginfo = context.getPackageManager().getPackageInfo
                    (context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pckginfo;
    }

    public static void showAcercaDe(final Context contexto) {
        PackageInfo pckginfo = getPackageInfo(contexto);
        assert pckginfo != null;
        String nombreVersion = pckginfo.versionName;

        String acerca=contexto.getString(R.string.menu_acercaDe);
        String nombreApp =contexto.getResources().getString(R.string.app_name);

        showDialog(
                contexto,
                acerca + " "+nombreApp,
                Html.fromHtml(
                        String.format(
                                contexto.getString(R.string.acerca_de_mensaje),
                                nombreVersion
                        )
                )
        );
    }

    public static Producto obtenerProducto(String json){

        Gson gson = new Gson();

        Producto producto = gson.fromJson(json,
                Producto.class);

        return producto;
    }

    public static Pedido obtenerPedido(String json){

        Gson gson = new Gson();

        Pedido pedido = gson.fromJson(json,
                Pedido.class);

        return pedido;
    }

    public static void enviarProducto(Producto producto){

    }
}
