package com.example.hp.compraqr.vendedor;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.baydev.compraqr.R;
import com.example.hp.compraqr.LoginActivity;

import com.example.hp.compraqr.adapters.DetallePedidoAdapter;
import com.example.hp.compraqr.entidades.Pedido;
import com.example.hp.compraqr.entidades.Producto;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Deque;

public class DetallePedido extends AppCompatActivity {

    static DetallePedidoAdapter adapter;
    RecyclerView recyclerView;

    private String idPedido;

    private FirebaseAuth mAuth;

    private Button btnDespachar;

    public DetallePedido() {
        adapter = new DetallePedidoAdapter();
        adapter.setDataset(new ArrayList<Producto>());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pedido);

        btnDespachar = findViewById(R.id.btnDespachar);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        //----------------------------------------------------------------
        Bundle extras = getIntent().getExtras();
        Serializable idRecibido = extras.getSerializable("ID_PEDIDO");
        if (extras!=null && idRecibido!=null){
            idPedido = idRecibido.toString();

            Log.e("ID_PEDIDO",idPedido);
        }
        //----------------------------------------------------------------

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference pedidosRef = mDatabase.getReference("pedidos/"+idPedido+"/productos");

        pedidosRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot producSnapshot: snapshot.getChildren()) {

                    Producto producto = producSnapshot.getValue(Producto.class);

                    agregarProducto(producto);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference pedidosRefe = mDatabase.getReference("pedidos/"+idPedido);



        pedidosRefe.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //boton para cambiar de estado el pedido
                btnDespachar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            pedidosRefe.child("estadoPed").setValue(true);
                            startActivity(new Intent(DetallePedido.this,ListaPedidos.class));

                    }

                });
                //--------------------------------------------------------------
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public static void agregarProducto(Producto producto){
        adapter.agregar(producto);
    }
}
