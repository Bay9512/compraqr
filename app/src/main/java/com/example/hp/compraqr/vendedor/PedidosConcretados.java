package com.example.hp.compraqr.vendedor;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.baydev.compraqr.R;

import com.example.hp.compraqr.adapters.ListaPedidosAdapter;
import com.example.hp.compraqr.entidades.Pedido;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.hp.compraqr.vendedor.DetallePedido.adapter;

public class PedidosConcretados extends AppCompatActivity {

    static ListaPedidosAdapter adapter;
    RecyclerView recyclerView;

    private FirebaseAuth mAuth;



    public PedidosConcretados() {
        adapter = new ListaPedidosAdapter();
        adapter.setDataset(new ArrayList<Pedido>());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_concretados);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        //--------------------------------------------------------

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference pedidosRef = mDatabase.getReference("pedidos");

        pedidosRef
                .orderByChild("estadoPed")
                .equalTo(true)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        for (DataSnapshot pedidosSnapshot: snapshot.getChildren()) {

                            Pedido pedido = pedidosSnapshot.getValue(Pedido.class);

                            pedido.setUid(pedidosSnapshot.getKey());

                            String nombreUsuario = pedido.getUsuario();
                            String dirEnvio = pedido.getDirEnvio();

                            agregarPedido(pedido);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public static void agregarPedido(Pedido pedido){
        adapter.agregar(pedido);
    }
}
