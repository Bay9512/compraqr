package com.example.hp.compraqr;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.baydev.compraqr.R;
import com.example.hp.compraqr.fragments.InicioFragment;
import com.example.hp.compraqr.fragments.CarritoFragment;
import com.example.hp.compraqr.fragments.CuentaFragment;

import static com.example.hp.compraqr.utils.Utils.showAcercaDe;


public class MainActivity extends AppCompatActivity {
    public static final String TAG_INICIO = "TAG_INICIO";
    public static final String TAG_CARRITO = "TAG_CARRITO";
    public static final String TAG_CUENTA = "TAG_CUENTA";


    private BottomNavigationView navegacion;
    private FrameLayout mainFrame;

    private InicioFragment inicioFragment;
    private CarritoFragment carritoFragment;
    private CuentaFragment cuentaFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navegacion = findViewById(R.id.navegacion);
        mainFrame = findViewById(R.id.mainFrame);

        inicioFragment=new InicioFragment();
        carritoFragment=new CarritoFragment();
        cuentaFragment=new CuentaFragment();

        navegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_inicio:
                        setFragment(inicioFragment,TAG_INICIO);
                        return true;
                    case R.id.nav_carrito:
                        setFragment(carritoFragment,TAG_CARRITO);

//                        getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame,CarritoFragment.instantiate(MainActivity.this,TAG_CARRITO)).commit();
                        return true;
                    case R.id.nav_cuenta:
                        setFragment(cuentaFragment,TAG_CUENTA);
                        return true;
                    default:
                        return false;
                }
            }
        });

        navegacion.setSelectedItemId(R.id.nav_inicio);
    }

    private void setFragment(Fragment fragment, String tag){
        getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame,fragment,tag).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acercaDe:
                showAcercaDe(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
