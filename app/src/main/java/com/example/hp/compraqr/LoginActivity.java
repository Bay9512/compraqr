package com.example.hp.compraqr;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.baydev.compraqr.R;
import com.example.hp.compraqr.entidades.Usuario;
import com.example.hp.compraqr.vendedor.ListaPedidos;
import com.facebook.FacebookSdk;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private Button btnIngresar;
    private EditText etCorreoL;
    private EditText etContraL;
    private ImageButton ibtnRegister;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.setApplicationId("226888717921165");

        FacebookSdk.sdkInitialize(this);

        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        btnIngresar = findViewById(R.id.btnIngreso);
        etCorreoL = findViewById(R.id.etCorreoL);
        etContraL = findViewById(R.id.etContraL);
        ibtnRegister = findViewById(R.id.imageButtonRegister);


        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        doLogin();
                    }
                }, 500);
            }
        });

        ibtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        Intent mainIntent = new Intent(LoginActivity.this,RegisterActivity.class);
                        startActivity(mainIntent);
                    }
                }, 500);
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {
                if(mAuth.getCurrentUser()!=null){
                    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
                    final DatabaseReference currentUserDB = mDatabase.getReference("users/"+mAuth.getCurrentUser().getUid());

                    currentUserDB.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            Usuario post = dataSnapshot.getValue(Usuario.class);
                            System.out.println(post);

                            //Log.d("zman",post.isTipo()+"");

                            if (firebaseAuth.getCurrentUser() != null && !post.isTipo()) {
                                Toast.makeText(LoginActivity.this, "Ahora estas logueado " + firebaseAuth.getCurrentUser().getDisplayName(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                                //mAuth.signOut();
                            }
                            else{
                                Toast.makeText(LoginActivity.this, "Ahora estas logueado " + firebaseAuth.getCurrentUser().getDisplayName(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LoginActivity.this, ListaPedidos.class);
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    private void doLogin() {
        String email = etCorreoL.getText().toString().trim();
        String password = etContraL.getText().toString().trim();

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, "Login exitoso", Toast.LENGTH_SHORT).show();
                            } else
                                Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }
}
