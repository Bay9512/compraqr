package com.example.hp.compraqr.entidades;

import java.io.Serializable;
import java.util.List;

public class ListaProductos implements Serializable {
    List<Producto> lista;

    public ListaProductos(List<Producto> lista) {
        this.lista = lista;
    }

    public List<Producto> getLista() {
        return lista;
    }

    public void setLista(List<Producto> lista) {
        this.lista = lista;
    }
}
