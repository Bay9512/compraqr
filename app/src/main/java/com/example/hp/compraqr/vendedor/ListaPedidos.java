package com.example.hp.compraqr.vendedor;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.baydev.compraqr.R;
import com.example.hp.compraqr.LoginActivity;

import com.example.hp.compraqr.adapters.ListaPedidosAdapter;
import com.example.hp.compraqr.entidades.Pedido;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.hp.compraqr.utils.Utils.showAcercaDe;


public class ListaPedidos extends AppCompatActivity {

    static ListaPedidosAdapter adapter;
    RecyclerView recyclerView;

    private FirebaseAuth mAuth;



    public ListaPedidos() {
        adapter = new ListaPedidosAdapter();
        adapter.setDataset(new ArrayList<Pedido>());
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lista_pedidos);



        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        //--------------------------------------------------------

        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference pedidosRef = mDatabase.getReference("pedidos");

        pedidosRef
                .orderByChild("estadoPed")
                .equalTo(false)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot pedidosSnapshot: snapshot.getChildren()) {

                    Pedido pedido = pedidosSnapshot.getValue(Pedido.class);

                    pedido.setUid(pedidosSnapshot.getKey());

                    String nombreUsuario = pedido.getUsuario();
                    String dirEnvio = pedido.getDirEnvio();

                    agregarPedido(pedido);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }


    public static void agregarPedido(Pedido pedido){
        adapter.agregar(pedido);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu2,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcSalir:
                mAuth = FirebaseAuth.getInstance();

                if (mAuth.getCurrentUser() != null) {
                    mAuth.signOut();
                    startActivity(new Intent(this, LoginActivity.class));
                }

                return true;
            case  R.id.opcPedEnviados:
                startActivity(new Intent(this, PedidosConcretados.class));
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
