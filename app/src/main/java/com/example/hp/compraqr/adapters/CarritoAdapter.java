package com.example.hp.compraqr.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.baydev.compraqr.R;
import com.example.hp.compraqr.entidades.Producto;

import java.util.ArrayList;
import java.util.List;

public class CarritoAdapter extends RecyclerView.Adapter<CarritoAdapter.VH> {

    private List<Producto> dataset;
    private View.OnClickListener clickListener;

    private Button btnMenos;
    private Button btnMas;

    public CarritoAdapter() {
        dataset = new ArrayList<>();
    }

    public void setDataset(List<Producto> dataset) {
        this.dataset = dataset;
        this.notifyDataSetChanged();
    }

    public List<Producto> getDatase(){
        return this.dataset;
    }


    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto,parent,false);
        view.setOnClickListener(clickListener);



        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.setData(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void agregar(Producto producto) {
        if(!dataset.contains(producto)) {
            this.dataset.add(producto);
            notifyDataSetChanged();
        }
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvNomProducto, tvCantidadProd, tvPrecioProd;

        private VH(View itemView) {
            super(itemView);
            tvNomProducto = itemView.findViewById(R.id.tvNomProducto);
            tvCantidadProd = itemView.findViewById(R.id.tvCantidadProd);
            tvPrecioProd = itemView.findViewById(R.id.tvPrecioProd);

            btnMenos = itemView.findViewById(R.id.btnMenos);
            btnMas = itemView.findViewById(R.id.btnMas);
        }

        public void setData(final Producto producto) {

            tvNomProducto.setText(producto.getNombreProd());
            tvCantidadProd.setText(String.valueOf(producto.getCantidadProd()));
            tvPrecioProd.setText(String.valueOf(producto.getPrecioProd()));
            final double precioUnidad = producto.getPrecioProd();

            //desactivo botones por el momento
            btnMas.setEnabled(false);
            btnMenos.setEnabled(false);


            btnMenos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int valor;
                    valor = Integer.parseInt(tvCantidadProd.getText().toString())-1;
                    producto.setCantidadProd(valor);
                    tvCantidadProd.setText(String.valueOf(valor));
                    double valor2;
                    valor2 = precioUnidad*valor;
                    tvPrecioProd.setText(String.valueOf(valor2));
                }
            });
            btnMas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int valor;
                    valor = Integer.parseInt(tvCantidadProd.getText().toString())+1;
                    producto.setCantidadProd(valor);
                    tvCantidadProd.setText(String.valueOf(valor));
                    double valor2;
                    valor2 = precioUnidad*valor;
                    tvPrecioProd.setText(String.valueOf(valor2));
                }
            });
        }
    }

    public void setOnClickListener(View.OnClickListener clickListener){
        this.clickListener = clickListener;
    }
}
