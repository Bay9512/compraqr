package com.example.hp.compraqr.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.baydev.compraqr.R;
import com.example.hp.compraqr.LoginActivity;

import com.example.hp.compraqr.RegisterActivity;
import com.example.hp.compraqr.adapters.CarritoAdapter;
import com.example.hp.compraqr.entidades.ListaProductos;
import com.example.hp.compraqr.entidades.Producto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CarritoFragment extends Fragment {
    static CarritoAdapter adapter;
    RecyclerView recyclerView;

    private Button btnComprar;
    private TextView tvTotalProds;

    public CarritoFragment() {
            adapter = new CarritoAdapter();
        adapter.setDataset(new ArrayList<Producto>());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_carrito, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        final List<Producto> listaProductos = adapter.getDatase();

        //eliminar producto
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                Toast.makeText(view.getContext(),viewHolder.getAdapterPosition()+"",Toast.LENGTH_SHORT).show();

                int posicion = Integer.parseInt(String.valueOf(viewHolder.getAdapterPosition()));

                listaProductos.remove(posicion);

                adapter.notifyItemRemoved(posicion);
                adapter.notifyItemRangeChanged(posicion,listaProductos.size());

            }
        }).attachToRecyclerView(recyclerView);
        ///////////////////////////////////////////////////////

        btnComprar = view.findViewById(R.id.btnComprar);
        tvTotalProds = view.findViewById(R.id.tvTotalProds);



        double precioTotal = 0;

        for(Producto objeto : listaProductos) {
            precioTotal += objeto.getPrecioProd();
        }

        tvTotalProds.setText(String.valueOf(precioTotal));


        btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PagarPedido.class);

                Bundle b = new Bundle();
                b.putSerializable("LISTA_PRODUCTOS",new ListaProductos(listaProductos));
                intent.putExtras(b);


                getActivity().startActivity(intent);
            }
        });



        return view;
}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static void agregarProducto(Producto producto){
        adapter.agregar(producto);
    }


}
