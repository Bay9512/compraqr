package com.example.hp.compraqr.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.baydev.compraqr.R;
import com.example.hp.compraqr.LoginActivity;
import com.example.hp.compraqr.MainActivity;

import com.example.hp.compraqr.RegisterActivity;
import com.example.hp.compraqr.adapters.CarritoAdapter;
import com.example.hp.compraqr.entidades.ListaProductos;
import com.example.hp.compraqr.entidades.Producto;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PagarPedido extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private RadioButton rbContraEntrega;
    private RadioButton rbTarjCred;
    private RadioButton rbPayPal;
    private EditText etDirEnvio;
    private EditText etCelular;
    private Button btnFinComp;
    private TextView tvTotalPago;
    private RadioGroup rGroup;



    private List<Producto> listaProductos;

    public PagarPedido() {
        listaProductos = new ArrayList<>();
    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagar_pedido);

        rbContraEntrega = findViewById(R.id.rbContraEntrega);
        rbTarjCred = findViewById(R.id.rbTarjCred);
        rbPayPal = findViewById(R.id.rbPayPal);
        tvTotalPago = findViewById(R.id.tvTotalPago);
        rGroup = findViewById(R.id.rGroup);

        //----------------------------------------------------------------
        Bundle extras = getIntent().getExtras();
        Serializable listaRecibida = extras.getSerializable("LISTA_PRODUCTOS");
        if (extras!=null && listaRecibida!=null){
            listaProductos = ((ListaProductos) listaRecibida).getLista();
        }
        //----------------------------------------------------------------

        double precioTotal = 0;

        for(Producto objeto : listaProductos) {
            precioTotal += objeto.getPrecioProd();
        }
        tvTotalPago.setText(String.valueOf(precioTotal));

//        //----------------------------------------------------------------
        //desactivo radiobuttons por el momento

        rbTarjCred.setEnabled(false);
        rbPayPal.setEnabled(false);

//        //----------------------------------------------------------------
//
//        String metodoPago = "-";
//
//        boolean rbContraEntregaEst = rbContraEntrega.isChecked();
//        //boolean rbTarjCredEst = rbTarjCred.isChecked();
//        boolean rbPayPalEst = rbPayPal.isChecked();
//
//        if (rbContraEntregaEst == true){
//            metodoPago = "Contra Entrega";
//        } else {
//            metodoPago = "Tarjeta de Credito";
//        }
//        if(rbPayPalEst == true){
//            metodoPago = "PayPal";
//        }
//
//        final String metodoPagoSel;
//        metodoPagoSel = metodoPago;
//
//        //----------------------------------------------------------------



        mAuth = FirebaseAuth.getInstance();

        btnFinComp = findViewById(R.id.btnFinComp);

        btnFinComp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //----------------------------------------------------------------

                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(PagarPedido.this);
                dialogo1.setTitle("Importante");
                dialogo1.setMessage("¿ Los datos ingresados son los correctos ?");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        aceptar();
                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.cancel();
                    }
                });
                dialogo1.show();

                //----------------------------------------------------------------

//                String metodoPago = "-";
//
//                boolean rbContraEntregaEst = rbContraEntrega.isChecked();
//                //boolean rbTarjCredEst = rbTarjCred.isChecked();
//                boolean rbPayPalEst = rbPayPal.isChecked();
//
//                if (rbContraEntregaEst == true){
//                    metodoPago = "Contra Entrega";
//                } else {
//                    metodoPago = "Tarjeta de Credito";
//                }
//                if(rbPayPalEst == true){
//                    metodoPago = "PayPal";
//                }
//
//                final String metodoPagoSel;
//                metodoPagoSel = metodoPago;
//
//                //----------------------------------------------------------------
//
//                String nombreProd;
//                int cantidadProd;
//
//                //----------------------------------------------------------------
//
//                etDirEnvio = findViewById(R.id.etDirEnvio);
//                String dirEnvio;
//                dirEnvio = etDirEnvio.getText().toString();
//
//                etCelular = findViewById(R.id.etCelular);
//                String nroCelular;
//                nroCelular = etCelular.getText().toString();
//
//                //----------------------------------------------------------------
//
//                double precioTotal = 0;
//                int i  = 0;
//
//                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("pedidos");
//                //DatabaseReference currentUserDB = mDatabase.child(mAuth.getCurrentUser().getUid());
//
//                DatabaseReference currentPedido = mDatabase.push();
//
//                for(Producto objeto : listaProductos) {
//
//                    nombreProd = objeto.getNombreProd();
//                    cantidadProd = objeto.getCantidadProd();
//
//                    precioTotal += objeto.getPrecioProd();
//
//                    DatabaseReference currentProdu = currentPedido.child("productos");
//
//                    DatabaseReference currentOrden = currentProdu.push();
//                    currentOrden.child("nombreProd").setValue(nombreProd);
//                    currentOrden.child("cantidadProd").setValue(cantidadProd);
//                    i++;
//                }
//
//
//
//                currentPedido.child("tipoEnvio").setValue(metodoPagoSel);
//                currentPedido.child("precioTotal").setValue(precioTotal);
//                currentPedido.child("dirEnvio").setValue(dirEnvio);
//                currentPedido.child("nroCelular").setValue(nroCelular);
//                currentPedido.child("estadoPed").setValue(false);
//                currentPedido.child("usuario").setValue(mAuth.getCurrentUser().getDisplayName());
//                currentPedido.child("idUsuario").setValue(mAuth.getCurrentUser().getUid());
//
//
//                Intent intent = new Intent(PagarPedido.this, MainActivity.class);
//                startActivity(intent);
//                finish();

            }

            public void aceptar() {
                String metodoPago = "-";

                boolean rbContraEntregaEst = rbContraEntrega.isChecked();
                //boolean rbTarjCredEst = rbTarjCred.isChecked();
                boolean rbPayPalEst = rbPayPal.isChecked();

                if (rbContraEntregaEst == true){
                    metodoPago = "Contra Entrega";
                } else {
                    metodoPago = "Tarjeta de Credito";
                }
                if(rbPayPalEst == true){
                    metodoPago = "PayPal";
                }

                final String metodoPagoSel;
                metodoPagoSel = metodoPago;

                //----------------------------------------------------------------

                String nombreProd;
                int cantidadProd;

                //----------------------------------------------------------------

                etDirEnvio = findViewById(R.id.etDirEnvio);
                String dirEnvio;
                dirEnvio = etDirEnvio.getText().toString();

                etCelular = findViewById(R.id.etCelular);
                String nroCelular;
                nroCelular = etCelular.getText().toString();

                //----------------------------------------------------------------

                double precioTotal = 0;
                int i  = 0;

                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("pedidos");
                //DatabaseReference currentUserDB = mDatabase.child(mAuth.getCurrentUser().getUid());

                DatabaseReference currentPedido = mDatabase.push();

                for(Producto objeto : listaProductos) {

                    nombreProd = objeto.getNombreProd();
                    cantidadProd = objeto.getCantidadProd();

                    precioTotal += objeto.getPrecioProd();

                    DatabaseReference currentProdu = currentPedido.child("productos");

                    DatabaseReference currentOrden = currentProdu.push();
                    currentOrden.child("nombreProd").setValue(nombreProd);
                    currentOrden.child("cantidadProd").setValue(cantidadProd);
                    i++;
                }

                currentPedido.child("tipoEnvio").setValue(metodoPagoSel);
                currentPedido.child("precioTotal").setValue(precioTotal);
                currentPedido.child("dirEnvio").setValue(dirEnvio);
                currentPedido.child("nroCelular").setValue(nroCelular);
                currentPedido.child("estadoPed").setValue(false);
                currentPedido.child("usuario").setValue(mAuth.getCurrentUser().getDisplayName());
                currentPedido.child("idUsuario").setValue(mAuth.getCurrentUser().getUid());


                //dialogo de confirmacion de contacto

                AlertDialog.Builder dialogo2 = new AlertDialog.Builder(PagarPedido.this);
                dialogo2.setTitle("Importante");
                dialogo2.setMessage("Nos contactaremos con usted para confirmar el pedido. Gracias.");
                dialogo2.setCancelable(false);
                dialogo2.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Intent intent = new Intent(PagarPedido.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialogo2.show();

                //-------------------------------------

//                Intent intent = new Intent(PagarPedido.this, MainActivity.class);
//                startActivity(intent);
//                finish();
            }
        });


    }
}
