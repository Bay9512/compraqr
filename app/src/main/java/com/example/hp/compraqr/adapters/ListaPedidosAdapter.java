package com.example.hp.compraqr.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.baydev.compraqr.R;

import com.example.hp.compraqr.entidades.Pedido;
import com.example.hp.compraqr.vendedor.DetallePedido;

import java.util.ArrayList;
import java.util.List;

public class ListaPedidosAdapter extends RecyclerView.Adapter<ListaPedidosAdapter.VH> {

    private List<Pedido> dataset;
    private View.OnClickListener clickListener;

    private Context context;

    public ListaPedidosAdapter() {
        dataset = new ArrayList<>();
    }

    public void setDataset(List<Pedido> dataset) {
        this.dataset = dataset;
        this.notifyDataSetChanged();
    }

    public List<Pedido> getDatase(){
        return this.dataset;
    }

    @NonNull
    @Override
    public ListaPedidosAdapter.VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuarios_pedidos,parent,false);
        view.setOnClickListener(clickListener);

        return new ListaPedidosAdapter.VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListaPedidosAdapter.VH holder, int position) {
        holder.setData(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }



    public void agregar(Pedido pedido) {
        if(!dataset.contains(pedido)) {
            this.dataset.add(pedido);
            notifyDataSetChanged();
        }
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvNomUsuario, tvDirEnvio;
        private Button btnDetalles;

        private VH(View itemView) {
            super(itemView);
            tvNomUsuario = itemView.findViewById(R.id.tvNomUsuario);
            tvDirEnvio = itemView.findViewById(R.id.tvDirEnvio);

            btnDetalles = itemView.findViewById(R.id.btnDetalles);
        }

        public void setData(final Pedido pedido) {

            tvNomUsuario.setText(pedido.getUsuario());
            tvDirEnvio.setText(String.valueOf(pedido.getDirEnvio()));

            btnDetalles.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetallePedido.class);

                    intent.putExtra("ID_PEDIDO",pedido.getUid());

                    context.startActivity(intent);

                    //Log.e("ID_PEDIDO",pedido.getUid());
                }
            });
        }
    }

    public void setOnClickListener(View.OnClickListener clickListener){
        this.clickListener = clickListener;
    }


}
