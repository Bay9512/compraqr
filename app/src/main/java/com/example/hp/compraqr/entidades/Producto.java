package com.example.hp.compraqr.entidades;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class Producto implements Serializable{
    private String nombreProd;
    private int cantidadProd;
    private double precioProd;

    public String getNombreProd() {
        return nombreProd;
    }

    public void setNombreProd(String nombreProd) {
        this.nombreProd = nombreProd;
    }

    public int getCantidadProd() {
        return cantidadProd;
    }

    public void setCantidadProd(int cantidadProd) {
        this.cantidadProd = cantidadProd;
    }

    public double getPrecioProd() {
        return precioProd;
    }

    public void setPrecioProd(double precioProd) {
        this.precioProd = precioProd;
    }

    @Override
    public String toString() {
        return nombreProd+cantidadProd+precioProd;
    }

    @Override
    public boolean equals(Object obj) {
        return this.nombreProd.equals(((Producto) obj).getNombreProd());
    }
}
